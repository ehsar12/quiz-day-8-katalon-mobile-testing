import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Catalog_Page/menu_bar'), 0)

Mobile.tap(findTestObject('Catalog_Page/menu login'), 0)

Mobile.verifyElementExist(findTestObject('Login_Page/btn_login'), 0)

for (int i = 1; i <= findTestData('login_data').getRowNumbers(); i++) {
    Mobile.setText(findTestObject('Login_Page/txt_username'), findTestData('login_data').getValue(1, i), 0)

    Mobile.setText(findTestObject('Login_Page/txt_password'), findTestData('login_data').getValue(2, i), 0)

    Mobile.tap(findTestObject('Login_Page/btn_login'), 0)

//    if (Mobile.verifyElementText(findTestObject('Object Repository/Login_Page/notif_username_required'), 'Username is required')) {
//        assert true
//    } else if (Mobile.verifyElementExist(findTestObject('Object Repository/Catalog_Page/btn_go_shopping'), 1)) {
//        assert true
//    } else if (Mobile.verifyElementText(findTestObject('Object Repository/Login_Page/notif_login_failed'), 'Provided credentials do not match any user in this service.')) {
//        assert true
//    } else {
//        assert false
//    }
}

